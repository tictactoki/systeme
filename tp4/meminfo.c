#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include "meminfo.h"


void meminfo_display(struct mem_info _info){
  printf("\t ----MEM_INFO----\n\n");
  printf("\tMemTotal:\t %lu KB\n",_info.kb_mem_total);
  printf("\tMemFree:\t %lu KB\n",_info.kb_mem_free);
  printf("\tBuffers:\t %lu KB\n",_info.kb_buffers);
  printf("\tCached:\t\t %lu KB\n",_info.kb_cached);
  printf("\tSwapCached:\t %lu KB\n",_info.kb_swap_cached);
  printf("\tActive:\t\t %lu KB\n",_info.kb_mem_active);
  printf("\tInactive:\t %lu KB\n",_info.kb_mem_inactive);
  printf("\tSwapTotal:\t %lu KB\n",_info.kb_swap_total);
  printf("\tSwapFree:\t %lu KB\n\n",_info.kb_swap_free);
 
}

struct mem_info mem_info_init(){
  
  struct mem_info _mem_info;

  _mem_info.kb_mem_total = 0;
  _mem_info.kb_mem_free = 0;
  _mem_info.kb_buffers = 0;
  _mem_info.kb_cached = 0;
  _mem_info.kb_swap_cached = 0;
  _mem_info.kb_mem_active = 0;
  _mem_info.kb_mem_inactive = 0;
  _mem_info.kb_swap_total = 0;
  _mem_info.kb_swap_free = 0;

  return _mem_info;

}


/*
  Parse line pour remplir mem_info
 */
void mem_info_from_str(char *line, struct mem_info * _memo_info){

  unsigned long value = 0;
  char name[NAMESIZE];
  
  memset(name,0,NAMESIZE);

  if(sscanf(line,"%s %lu",name,&value) != 2){
    perror("sscanf mem_info\n");
    exit(2);
  }

  if(strncmp(name,"MemTotal:",strlen(name)) == 0){
    _memo_info->kb_mem_total = value;
    return;
  }
  if(strncmp(name,"MemFree:",strlen(name)) == 0){
    _memo_info->kb_mem_free = value;
    return;
  }
  if(strncmp(name,"Buffers:",strlen(name)) == 0){
    _memo_info->kb_buffers = value;
    return;
  }
  if(strncmp(name,"Cached:",strlen(name)) == 0){
    _memo_info->kb_cached = value;
    return;
  }
  if(strncmp(name,"SwapCached:",strlen(name)) == 0){
    _memo_info->kb_swap_cached = value;
    return;
  }
  if(strncmp(name,"Active:",strlen(name)) == 0){
    _memo_info->kb_mem_active = value;
    return;
  }
  if(strncmp(name,"Inactive:",strlen(name)) == 0){
    _memo_info->kb_mem_inactive = value;
    return;
  }
  if(strncmp(name,"SwapTotal:",strlen(name)) == 0){
    _memo_info->kb_swap_total = value;
    return;
  }
  if(strncmp(name,"SwapFree:",strlen(name)) == 0){
    _memo_info->kb_swap_free = value;
    return;
  }
  return;
}

void mem_info_from_file(struct mem_info * _mem_info){
  
  FILE * file = NULL;
  char * line = NULL;
  size_t length = 0;
  ssize_t read;
  
  file = fopen(MEMINFO,"r");

  if(file == NULL){
    perror("open file meminfo\n");
    exit(2);
  }
  // parcours du fichier
  while((read = getline(&line,&length,file)) != -1){
    mem_info_from_str(line,_mem_info);
    free(line);
    line = NULL;
    length = 0;
  }
  // calcul pourcentage
  fclose(file);

}

void mem_info_percentage(struct mem_info _mem_info, struct percentage_info * percentage){
  
  double per_mem_active,per_mem_free,per_mem_inactive;

  per_mem_active =((double)_mem_info.kb_mem_active / (double)_mem_info.kb_mem_total) * 100;
  per_mem_free = ((double)(_mem_info.kb_mem_free) / (double)_mem_info.kb_mem_total) * 100;
  per_mem_inactive = ((double)_mem_info.kb_mem_inactive / (double)_mem_info.kb_mem_total) * 100;

  percentage->per_mem_active = per_mem_active;
  percentage->per_mem_free = per_mem_free;
  percentage->per_mem_inactive = per_mem_inactive;


}
