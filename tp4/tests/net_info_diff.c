#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "../net_info.h"

int main(int argc, char** argv){
  struct net_info net1;
  struct net_info net2;
  struct net_info result;

  net2.recv_bytes = 201893;
  net2.recv_packets = 183;
  net2.trans_bytes = 39309903;
  net2.trans_packets = 3271;

  net1.recv_bytes = 20487;
  net1.recv_packets = 34;
  net1.trans_bytes = 638299;
  net1.trans_packets = 484;

  result = net_info_diff(net2, net1);

  assert(result.recv_bytes == (net2.recv_bytes - net1.recv_bytes)
	 && result.recv_packets == (net2.recv_packets - net1.recv_packets)
	 && result.trans_bytes == (net2.trans_bytes - net1.trans_bytes)
	 && result.trans_packets == (net2.trans_packets - net1.trans_packets));

  return 0;
}

