#include <stdio.h>
#include <stdlib.h>
#include "monitoring.h"
#include "net_info.h"
#include "rstat.h"

/*void monitoring_destroy(struct monitoring_info * monitoring){
  rstat_destroy(&monitoring->_tab_rstat);
  net_info_destroy(&(monitoring->n_info));
		     //free(monitoring);
		     }*/

void monitoring_display(struct monitoring_info monit_info){
  
  meminfo_display(monit_info._mem_info);
  

}

struct monitoring_info monitoring_init(void){
  
  struct monitoring_info monitoring;
  
  monitoring._mem_info = mem_info_init();
  monitoring._proc_and_rstat = proc_and_rstat_init();
  net_info_init_fields(&monitoring.nt_info);

  return monitoring;

}

int monitoring_from_files(struct monitoring_info* monit_info){
  
  proc_and_rstat_from_files(&(monit_info->_proc_and_rstat));
  mem_info_from_file(&(monit_info->_mem_info));
  net_info_from_file(&(monit_info->nt_info));

  return 1;
  // net_info_init_from_file(&(monit_info->nt_info));
}

struct monitoring_info monitoring_diff(struct monitoring_info last, struct monitoring_info previous){
  //struct monitoring_info diff_result;
  //monitoring_init(&diff_result);
  struct monitoring_info diff_result = monitoring_init();

  

  //TODO: faire la diff entre les autres structures dont les valeurs comprennent
  //les valeurs des donnees precedentes
  
  diff_result.nt_info = net_info_diff(last.nt_info, previous.nt_info);

  return diff_result;
}

void monitoring_destroy(struct monitoring_info* monit_info){
  //TODO: appeler les destroy des structures
}

