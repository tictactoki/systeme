#ifndef __RSTAT_H__
#define __RSTAT_H__
#include "utils.h"
#define PROC "/proc"

// /proc/pid/stat
struct rstat {
  unsigned long utime,
    stime,
    cutime,
    cstime,
    kb_mem_rss,
    p_mem_rss;
    
};
// contenant toutes les infos sur les pids
struct tab_rstat {
  
  unsigned int counter;
  struct rstat * _rstat;

};

/*
  met les valeurs des champs à 0
 */
struct rstat rstat_init();

/*
  Ajoute les valeurs dans la structures
 */
void rstat_add(struct rstat * _rstat, unsigned long utime, unsigned long stime, unsigned long cutime,
	      unsigned long cstime, unsigned long p_mem_rss);

/*
  Remplit les champs de la structure
 */
void rstat_info_from_files(struct rstat * _rstat);

/*
  alloue l'espace necessaire pour stocker les données des répertoires pid
  @tab_rstat: structure tab_rstat
  @length: nombre de repertoire pid
  @return: 1 si l'allocation à réussi, 0 sinon
 */
int rstat_resize(struct tab_rstat * _tab_rstat,unsigned int length);

/*
  libère la mémoire utilisée par tab_rstat
  @tab_rstat: structure tab_rstat
 */
void rstat_destroy(struct tab_rstat * _tab_rstat);

/*
  Affiche les données de la structure
 */
void rstat_display(struct rstat _rstat);

#endif
