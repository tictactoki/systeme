#include <stdio.h>
#include <stdlib.h>
#include "proc_and_rstat.h"


struct proc_and_rstat proc_and_rstat_init(){
  
  struct proc_and_rstat _stat;

  _stat._proc_stat = proc_stat_init();
  _stat._rstat = rstat_init();

  return _stat;

}


void proc_and_rstat_from_files(struct proc_and_rstat * _stat){
  
  rstat_info_from_files(&(_stat->_rstat));
  proc_stat_from_file(&(_stat->_proc_stat));

}


void proc_and_rstat_percentage(struct percentage_info * info,struct proc_and_rstat last, struct proc_and_rstat previous){
  
  double total,sys,user,cpu_usage;

  total = proc_stat_get_total(last._proc_stat) - proc_stat_get_total(previous._proc_stat);
  
  user = proc_stat_get_diff(last._rstat.utime,previous._rstat.utime);
  sys = proc_stat_get_diff(last._rstat.stime,previous._rstat.stime);

  cpu_usage = user + sys;
  
  cpu_usage = 100 * cpu_usage / total;
  user = 100 * user / total;
  sys = 100 * sys / total;

  info->per_cpu_total = cpu_usage;
  info->per_cpu_user = user;
  info->per_cpu_sys = sys;

}
