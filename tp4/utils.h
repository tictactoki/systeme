#ifndef __UTIL_H__
#define __UTIL_H__
#define _GNU_SOURCE_
#define NAMESIZE 128
#define BUFFSIZE 8192
#define RSTATSIZE 32

// facteur multiplicatif des données des pages en KB
#define kb_per_page (sysconf(_SC_PAGESIZE) / 1024)

#define clkt_per_sec sysconf(_SC_CLK_TCK)

#define FIELD_DIFF_AFFECT(diff,st2,st1,field_name) diff.field_name=st2.field_name-st1.field_name

/*
  Test si la chaine est un répertoire pid
  @number: chaine contenant eventuellement des nombres
  @return: 1 si c'est un répertoire pid, 0 sinon
*/
int is_number(char * number);

/*
  Pour des données en pourcentage
 */
struct percentage_info {

  double per_mem_active,
    per_mem_free,
    per_mem_inactive,
    per_cpu_total,
    per_cpu_user,
    per_cpu_sys;

};

/*
  Initialise les données de la structure
 */
struct percentage_info percentage_info_init(void);

  
void percentage_display_name(void);

void percentage_display(struct percentage_info info);

#endif
