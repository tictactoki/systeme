#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include "procstat.h"
#include "utils.h"

struct proc_stat proc_stat_init(){
  
  struct proc_stat _stat;
  
  _stat.cpu_user = 0;
  _stat.cpu_nice = 0;
  _stat.cpu_sys = 0;
  _stat.cpu_idle = 0;
  _stat.iowait = 0;
  _stat.irq = 0;
  _stat.softirq = 0;
  _stat.steal = 0;
  _stat.guest = 0;
  _stat.guest_nice = 0;
  _stat.intr = 0;
  _stat.ctxt = 0;
  _stat.btime = 0;
  _stat.per_cpu = 0;
  _stat.per_user = 0;
  _stat.per_sys = 0;
  _stat.per_nice = 0;

  return _stat;

}


void proc_stat_display(struct proc_stat _stat){
  printf("\t ----PROC_STAT----\n\n");
  printf("\tCpuUser:\t %lu \n",_stat.cpu_user);
  printf("\tCpuNice:\t %lu \n",_stat.cpu_nice);
  printf("\tCpuSys\t\t %lu \n",_stat.cpu_sys);
  printf("\tCpuIdle:\t %lu \n",_stat.cpu_idle);
  printf("\tIowait:\t\t %lu \n",_stat.iowait);
  printf("\tIrq:\t\t %lu \n",_stat.irq);
  printf("\tSoftIrq:\t %lu \n",_stat.softirq);
  printf("\tSteal:\t\t %lu \n",_stat.steal);
  printf("\tGuest:\t\t %lu \n",_stat.guest);
  printf("\tGuestNice:\t %lu \n",_stat.guest_nice);
  printf("\tIntr:\t\t %lu \n",_stat.intr);
  printf("\tCtxt:\t\t %lu \n",_stat.ctxt);
  printf("\tBTime:\t\t %lu \n\n",_stat.btime);
}


void proc_stat_from_file(struct proc_stat * _stat){
  
  //struct proc_stat stat;
  char buf[BUFFSIZE];
  char * string;
  char tmp[NAMESIZE];
  int fd;

  fd = open(PROCSTAT, O_RDONLY);
  
  if(fd == -1){
    perror("open /proc/stat\n");
    exit(7);
  }

  memset(buf,0,BUFFSIZE);
  memset(tmp,0,NAMESIZE);

  read(fd,buf,BUFFSIZE-1);

  string = strstr(buf,"cpu");

  if(string == NULL){
    perror("strstr cpu\n");
    exit(8);
  }

  if (sscanf(string,
	     "%s %lu %lu %lu %lu"
	     "%lu %lu %lu %lu"
	     "%lu %lu"
	     ,tmp,&_stat->cpu_user,&_stat->cpu_nice,&_stat->cpu_sys,&_stat->cpu_idle
	     ,&_stat->iowait,&_stat->irq,&_stat->softirq,&_stat->steal
	     ,&_stat->guest,&_stat->guest_nice) != 11){
    perror("sscanf cpu\n");
    exit(9);
  }

  string = strstr(buf,"intr");

  if(string == NULL){
    perror("strstr intr\n");
    exit(8);
  }

  if(sscanf(string,"%s %lu",tmp,&_stat->intr) != 2){
    perror("sscanf intr\n");
    exit(9);
  }

  string = strstr(buf,"ctxt");

  if(string == NULL){
    perror("strstr ctxt\n");
    exit(8);
  }

  if(sscanf(string,"%s %lu",tmp,&_stat->ctxt) != 2){
    perror("sscanf ctxt\n");
    exit(9);
  }

  string = strstr(buf,"btime");

  if(string == NULL){
    perror("strstr btime\n");
    exit(8);
  }

  if(sscanf(string,"%s %lu",tmp,&_stat->btime) != 2){
    perror("sscanf btime\n");
    exit(9);
  }

}

double proc_stat_get_diff(unsigned long last, unsigned long previous){
  return (double)last-previous;
}

double proc_stat_get_total(struct proc_stat _stat){
  return _stat.cpu_user + _stat.cpu_idle + _stat.cpu_nice + _stat.cpu_sys
    + _stat.iowait + _stat.irq + _stat.softirq;
}

void proc_stat_percentage(struct proc_stat last, struct proc_stat previous){
  
  double idle,per_cpu,per_sys,per_user,per_nice,total;

  idle = last.cpu_idle - previous.cpu_idle;
  total = proc_stat_get_total(last) - proc_stat_get_total(previous);
  per_user = proc_stat_get_diff(last.cpu_user,previous.cpu_user);
  per_sys = proc_stat_get_diff(last.cpu_sys,previous.cpu_sys);
  per_nice = proc_stat_get_diff(last.cpu_nice,previous.cpu_nice);


  per_cpu = 100 * (total - idle) / total;
  per_user = 100 * per_user / total;
  per_nice = 100 * per_nice / total;
  per_sys = 100 * per_sys / total;

  printf("cpu:%lf%% user:%lf%% nice:%lf%% sys:%lf%%\n",per_cpu,per_user,per_nice,per_sys);


}
