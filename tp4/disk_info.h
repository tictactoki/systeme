#ifndef __DISK_INFO_H__
#define __DISK_INFO_H__

#define DISK_INFO_FIELDS_NB 14

#define DISK_FILE_PATH "/proc/diskstats"

struct disk_info {
  unsigned long long readings;
  unsigned long long writings;
};

void disk_info_init_fields(struct disk_info* d_info);

int disk_info_from_file(struct disk_info* d_info);

struct disk_info disk_info_diff(struct disk_info last, struct disk_info previous);

void disk_info_display(struct disk_info d_info);

#endif

