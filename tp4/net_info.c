#include <stdio.h>
#include <stdlib.h>

#include "net_info.h"
#include "utils.h"

void net_info_init_fields(struct net_info* n_info){
  n_info->recv_packets = 0;
  n_info->recv_bytes = 0;
  n_info->trans_packets = 0;
  n_info->trans_bytes = 0;
}

int net_info_from_file(struct net_info* n_info){
  FILE* net_file = fopen(NET_FILE_PATH, "r");
  char* line = NULL;
  size_t size = 0;
  ssize_t read_bytes = 0;
  char dev_name[256];
  unsigned long long useless_field = 0;
  unsigned long long recv_bytes, recv_packets, trans_bytes, trans_packets;
  int i;

  net_info_init_fields(n_info);

  if(net_file == NULL){
    return 0;
  }
  
  //Lecture des 2 premieres lignes qui ne contiennent aucune information
  for(i=0; i<2; i++){
    read_bytes = getline(&line, &size, net_file);

    if(read_bytes <= 0){
      return 0;
    }

    free(line);
    size = 0;
    line = NULL;
  }

  while(1){
    read_bytes = getline(&line, &size, net_file);

    if(read_bytes == -1){
      return 0;
    }

    if(read_bytes == 0){
      break;
    }

    //Traitement de la ligne
    if(sscanf(line, "%s %llu %llu %llu %llu %llu %llu %llu %llu %llu %llu %llu %llu %llu %llu %llu %llu",
	      dev_name, 
	      &recv_bytes, &recv_packets,
	      &useless_field, &useless_field, &useless_field, &useless_field,
	      &useless_field, &useless_field,
	      &trans_bytes, &trans_packets,
	      &useless_field, &useless_field, &useless_field, &useless_field,
	      &useless_field, &useless_field) == NET_INFO_FIELDS_NB){
      n_info->recv_bytes += recv_bytes;
      n_info->recv_packets += recv_packets;
      n_info->trans_bytes += trans_bytes;
      n_info->trans_packets += trans_packets;
    }

    free(line);
    size = 0;
    line = NULL;
  }

  fclose(net_file);
  return 1;
}

struct net_info net_info_diff(struct net_info last, struct net_info previous){
  struct net_info diff_result;

  FIELD_DIFF_AFFECT(diff_result, last, previous, recv_packets);
  FIELD_DIFF_AFFECT(diff_result, last, previous, recv_bytes);
  FIELD_DIFF_AFFECT(diff_result, last, previous, trans_packets);
  FIELD_DIFF_AFFECT(diff_result, last, previous, trans_bytes);
  
  return diff_result;
}

void net_info_display(struct net_info n_info){
  printf("(NET) { rc_b=%llu, rc_pck=%llu, tr_b=%llu, tr_pck=%llu }\n",
	 n_info.recv_bytes, n_info.recv_packets, 
	 n_info.trans_bytes, n_info.trans_packets);
}


