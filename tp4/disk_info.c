#include <stdio.h>
#include <stdlib.h>

#include "disk_info.h"
#include "utils.h"

void disk_info_init_fields(struct disk_info* d_info){
  d_info->readings = 0;
  d_info->writings = 0;
}

int disk_info_from_file(struct disk_info* d_info){
  FILE* disk_file = fopen(DISK_FILE_PATH, "r");
  char* line = NULL;
  size_t size = 0;
  ssize_t read_bytes = 0;
  char dev_name[256];
  unsigned long long useless_field = 0;
  unsigned long long readings, writings;

  disk_info_init_fields(d_info);

  if(disk_file == NULL){
    return 0;
  }

  while(1){
    read_bytes = getline(&line, &size, disk_file);

    if(read_bytes == -1){
      return 0;
    }

    if(read_bytes == 0){
      break;
    }

    //Traitement de la ligne
    if(sscanf(line, "%llu %llu %s %llu %llu %llu %llu %llu %llu %llu %llu %llu %llu %llu",
	      &useless_field, &useless_field, dev_name, 
	      &readings, 
	      &useless_field, &useless_field, &useless_field, 
	      &writings,
		  &useless_field, &useless_field, &useless_field,
		  &useless_field, &useless_field, &useless_field) == DISK_INFO_FIELDS_NB){
      d_info->readings += readings;
      d_info->writings += writings;
    }

    free(line);
    size = 0;
    line = NULL;
  }

  fclose(disk_file);
  return 1;
}

struct disk_info disk_info_diff(struct disk_info last, struct disk_info previous){
  struct disk_info diff_result;

  FIELD_DIFF_AFFECT(diff_result, last, previous, readings);
  FIELD_DIFF_AFFECT(diff_result, last, previous, writings);
  
  return diff_result;
}

void disk_info_display(struct disk_info d_info){
  printf("(DISK) { rd=%llu, wr=%llu }\n",
	 d_info.readings, d_info.writings);
}
