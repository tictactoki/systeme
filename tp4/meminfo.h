#ifndef __MEMINFO_H__
#define __MEMINFO_H__
#include "utils.h"

#define MEMINFO "/proc/meminfo"

// Données du fichier meminfo 
struct mem_info {
  unsigned long kb_mem_total,
    kb_mem_free,
    kb_buffers,
    kb_cached,
    kb_swap_cached,
    kb_mem_active,
    kb_mem_inactive,
    kb_swap_total,
    kb_swap_free;
};

/*
  Initialise les valeurs de la structures 
  @line: ligne courant du fichier lu
  @_mem_info: structure mem_info
 */
void mem_info_from_str(char *line, struct mem_info * _mem_info);

/*
  Initialise les valeurs de la structure
  @_mem_info: Structure mem_info
 */
void mem_info_from_file(struct mem_info * _mem_info);

/*
  @return: structure mem_info
 */
struct mem_info mem_info_init();

/*
  calcul les pourcentages des mémoire active,free,inactive
 */
void mem_info_percentage(struct mem_info _mem_info,struct percentage_info * percentage);


void meminfo_display(struct mem_info _info);


#endif
