#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "monitoring.h"
#include "core.h"


/* Indice de la prise courante */
int current_trace_index = 0;

/* Ensemble des prises */
struct monitoring_info traces[MAX_TRACES_NB > 1 ? MAX_TRACES_NB : 2];

void core_init(void){
  int i;

  for(i=0; i<MAX_TRACES_NB; i++){
    //monitoring_init(&traces[i]);
    traces[i] = monitoring_init();
  }
}

void* core_write_data(void* monit_info){
  struct monitoring_info* m_info = (struct monitoring_info*)monit_info;
  
  //TODO: ecriture de la donnee dans le fichier
  
  pthread_exit(NULL);
}

/* Fonction principale de l'outil de monitoring */
void core_process(void){
  struct monitoring_info last_info, current_info;
  unsigned long long total_time = 0;
  
  while(total_time < MONITORING_TIME){
    pthread_t thread;
  
    sleep(MONITORING_DELAY/1000);
    total_time += MONITORING_DELAY;
    
    monitoring_from_files(&traces[current_trace_index]);

    last_info = core_get_previous_trace();
    current_info = traces[current_trace_index];

    traces[current_trace_index] = monitoring_diff(current_info, last_info);
    monitoring_destroy(&current_info);

    pthread_create(&thread, NULL, core_write_data, &traces[current_trace_index]);

    current_trace_index = (current_trace_index + 1) % MAX_TRACES_NB;
  }
}

struct monitoring_info core_get_previous_trace(void){
  return traces[(current_trace_index - 1 + MAX_TRACES_NB) % MAX_TRACES_NB];
}

void core_destroy(void){
  int i;

  for(i=0; i<MAX_TRACES_NB; i++){
    monitoring_destroy(&traces[i]);
  }
}
