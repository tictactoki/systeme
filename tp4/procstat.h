#ifndef __PROC_STAT_H__
#define __PROC_STAT_H__

#define PROCSTAT "/proc/stat"

// donnée du fichier /proc/stat
struct proc_stat {
  unsigned long cpu_user,
    cpu_nice,
    cpu_sys,
    cpu_idle,
    iowait,
    irq,
    softirq,
    steal,
    guest,
    guest_nice,
    intr,
    ctxt,
    btime;
  double per_cpu,per_sys,per_user,per_nice;
  
};

/*
  Initialisation de structure proc_stat
 */
struct proc_stat proc_stat_init();

/*
  Remplit les champs de la structure
 */
void proc_stat_from_file(struct proc_stat * _stat);

/*
  Affiche les données de la structure
 */
void proc_stat_display(struct proc_stat _stat);

/*
  retourne l'utilisation total du cpu
 */
double proc_stat_get_total(struct proc_stat _stat);

void proc_stat_percentage(struct proc_stat last, struct proc_stat previous);

/*
  retourne la différence
 */
double proc_stat_get_diff(unsigned long last, unsigned long previous);

#endif
