#include <stdio.h>
#include <stdlib.h>
#include "utils.h"


/*
  Retourne 1 si c'est un nombre 0 sinon
 */
int is_number(char * number){

  char *endptr;
  long val = 0;
  val = strtol(number,&endptr,10);
  
  if(val <= 0 || endptr == number){
    return 0;
  }
  
  return 1;
}


struct percentage_info percentage_info_init(void){
  
  struct percentage_info percentage;

  percentage.per_mem_active = 0;
  percentage.per_mem_free = 0;
  percentage.per_mem_inactive = 0;
  percentage.per_cpu_total = 0;
  percentage.per_cpu_user = 0;
  percentage.per_cpu_sys = 0;

  return percentage;
  
}

void percentage_display_name(void){
  
  printf("MemActive \t CpuTotal \t CpuUser \t CpuSys\n");

}

void percentage_display(struct percentage_info info){
  
  printf("%lf%% \t %lf%% \t %lf%% \t %lf%%\n",info.per_mem_active,info.per_cpu_total,info.per_cpu_user,info.per_cpu_sys);

}
