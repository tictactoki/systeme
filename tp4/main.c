#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "monitoring.h"
#include "core.h"
#include "proc_and_rstat.h"

int main(int argc, char**argv){
  //TODO: decommenter cette partie pour lancer le programme
  /* core_init();
  core_process();
  core_destroy();
  */

  int i;
  percentage_display_name();
  for(i=0;i<10;i++){
    
    struct monitoring_info info = monitoring_init();
    struct percentage_info per = percentage_info_init();
    monitoring_from_files(&info);
    sleep(2);
    struct monitoring_info info2 = monitoring_init();
    monitoring_from_files(&info2);
    per = percentage_info_init();
    proc_and_rstat_percentage(&per,info._proc_and_rstat,info2._proc_and_rstat);
    mem_info_percentage(info2._mem_info,&per);
    percentage_display(per);
  }
  return 0;
}
