#ifndef __NET_INFO_H__
#define __NET_INFO_H__

/* Chemin vers le fichier contenant les informations */
#define NET_FILE_PATH "/proc/net/dev"

/* Nombre de champs par ligne */
#define NET_INFO_FIELDS_NB 17


struct net_info {
  unsigned long long recv_packets;
  unsigned long long recv_bytes;
  unsigned long long trans_packets;
  unsigned long long trans_bytes;
};

/* Initialise les champs a 0/NULL */
void net_info_init_fields(struct net_info* n_info);

/* Charge une structure a partir du fichier d'informations */
int net_info_from_file(struct net_info* n_info);

/* Renvoie la difference entre les differents champs de la structure */
struct net_info net_info_diff(struct net_info last, struct net_info previous);

/* Renvoie la derniere prise */
struct net_info net_info_get_previous_trace(void);

/* Ajoute la prise a la fin des prises */
void net_info_append_trace(struct net_info n_info);

/* Affiche le contenu d'une structure net_info sur la sortie standard */
void net_info_display(struct net_info n_info);

#endif
