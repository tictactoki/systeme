#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include "rstat.h"


void rstat_display(struct rstat _rstat){
  
  printf("\t ----RSTAT----\n\n");
  printf("\tUTime:\t\t %lu\n",_rstat.utime);
  printf("\tSTime:\t\t %lu\n",_rstat.stime);
  printf("\tCuTime:\t\t %lu\n",_rstat.cutime);
  printf("\tCsTime:\t\t %lu\n",_rstat.cstime);
  printf("\tRssPage:\t %lu\n",_rstat.p_mem_rss);
  printf("\tRssPageKB:\t %lu\n\n",_rstat.kb_mem_rss);

}


struct rstat rstat_init(){
  
  struct rstat _rstat;
  _rstat.utime = 0;
  _rstat.stime = 0;
  _rstat.cutime = 0;
  _rstat.cstime = 0;
  _rstat.p_mem_rss = 0;
  _rstat.kb_mem_rss = 0;

  return _rstat;

}

int rstat_resize(struct tab_rstat * _tab_rstat,unsigned int length){
  if(_tab_rstat->counter <= length){
    
    unsigned int new_length = _tab_rstat->counter + RSTATSIZE;
    printf("length: %d \n",new_length);
    struct rstat * _rstat = realloc(_tab_rstat->_rstat, sizeof(struct rstat) * new_length);
    
    if(_rstat == NULL){
      return 0;
    }

    _tab_rstat->_rstat = _rstat;
    _tab_rstat->counter = new_length;

  }
  
  return 1;

}

void rstat_destroy(struct tab_rstat * _tab_rstat){
  if(_tab_rstat->_rstat != NULL){
    free(_tab_rstat->_rstat);
  }
  _tab_rstat->counter = 0;
  
}

void rstat_add(struct rstat * _rstat, unsigned long utime, unsigned long stime, unsigned long cutime,
	      unsigned long cstime, unsigned long p_mem_rss){

  _rstat->utime += utime;
  _rstat->stime += stime;
  _rstat->cutime += cutime;
  _rstat->cstime += cstime;
  _rstat->p_mem_rss += p_mem_rss;
  
}


void rstat_info_from_files(struct rstat * _rstat){
  
  struct dirent *current;
  char buf[BUFFSIZE];
  char filename[NAMESIZE];
  char comm[NAMESIZE];
  char state;
  int fd;
  DIR * proc;
  unsigned long tmp; // variable temporaire
  unsigned long utime,stime,cutime,cstime,p_mem_rss;

  memset(buf,0,BUFFSIZE);
  memset(filename,0,NAMESIZE);

  if((proc=opendir(PROC))==NULL){
    perror("opendir proc\n");
    exit(4);
  }

  while((current=readdir(proc))){
    // teste si c'est un répertoire pid
    if(is_number(current->d_name)){
      
      memset(buf,0,BUFFSIZE);
      memset(filename,0,NAMESIZE);
      
      // ajoute à filename le nom du fichier
      snprintf(filename,NAMESIZE,"/proc/%s/stat",current->d_name);
      
      if((fd = open(filename,O_RDONLY)) != -1){
	read(fd,buf,BUFFSIZE-1);
	// stocke les valeurs dans la structure 
	if(sscanf(buf,"%lu %s %c %lu %lu %lu %lu"
		  "%lu %lu %lu %lu"
		  "%lu %lu %lu"
		  "%lu %lu %lu %lu"
		  "%lu %lu %lu %lu %lu"
		  "%lu",
		  &tmp,comm,&state,&tmp,&tmp,&tmp,&tmp
		  ,&tmp,&tmp,&tmp,&tmp
		  ,&tmp,&tmp
		  ,&utime,&stime,&cutime,&cstime
		  ,&tmp,&tmp,&tmp,&tmp,&tmp,&tmp
		  ,&p_mem_rss) != 24){
	  perror("sscanf /proc/pid/stat\n");
	  close(fd);
	  closedir(proc);
	  exit(6);
	}
	close(fd);
	rstat_add(_rstat,utime,stime,cutime,cstime,p_mem_rss);
      }
    }
  }

  closedir(proc);
  // page en kb
  _rstat->kb_mem_rss = _rstat->p_mem_rss * kb_per_page;
  
}
