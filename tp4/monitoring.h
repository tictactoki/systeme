#ifndef __MONITORING_H__
#define __MONITORING_H__
#include "proc_and_rstat.h"
#include "meminfo.h"
#include "net_info.h"
#include "disk_info.h"

// ensemble des structures
struct monitoring_info {
  
  struct mem_info _mem_info;
  struct proc_and_rstat _proc_and_rstat;
  struct net_info nt_info;
  struct disk_info dk_info;
};

/*
  Initialise la structure 
 */
struct monitoring_info monitoring_init(void);

int monitoring_from_files(struct monitoring_info* monit_info);

struct monitoring_info monitoring_diff(struct monitoring_info last, struct monitoring_info previous);

void monitoring_destroy(struct monitoring_info* monit_info);

void monitoring_display(struct monitoring_info monit_info);

#endif
