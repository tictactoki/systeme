#ifndef __MONIT_CORE_H__
#define __MONIT_CORE_H__

#include "monitoring.h"

/* Temps de monitoring en millisecondes : 15 minutes */
#define MONITORING_TIME (15*60*1000)

/* Delai de recuperation des donnees en millisecondes : 15 secondes */
#define MONITORING_DELAY 15000

/* Nombre de prises max */
#define MAX_TRACES_NB MONITORING_TIME/MONITORING_DELAY


/* Initialise l'outil de monitoring */
void core_init(void);

/* Fonction de traitement d'une prise */
void* core_write_data(void* monit_info);

/* Fonction principale de l'outil de monitoring */
void core_process(void);

/* Renvoie la prise precedente */
struct monitoring_info core_get_previous_trace(void);

/* Detruit les ressources utilisees par l'outil de monitoring */
void core_destroy(void);

#endif
