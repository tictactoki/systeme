#ifndef __PROC_AND_RSTAT_H__
#define __PROC_AND_RSTAT_H__
#include "procstat.h"
#include "rstat.h"

// pour rstat et proc/stat
struct proc_and_rstat {
  
  struct proc_stat _proc_stat;
  struct rstat _rstat;
  
};
/*
  Initialise les données de la structure
 */
struct proc_and_rstat proc_and_rstat_init(void);

/*
  Remplit les données à partir des fichiers.
 */
void proc_and_rstat_from_files(struct proc_and_rstat * _proc_ad_rstat);

/*
  Effectue les calculs pour percentage_info
 */
void proc_and_rstat_percentage(struct percentage_info * info,struct proc_and_rstat last, struct proc_and_rstat previous);



#endif
