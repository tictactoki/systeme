#include <sys/types.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <errno.h>

char cmd[ARG_MAX]={0x0};

int dump(void)
{
		struct rusage s;
		struct rusage *p=&s;
		
  	getrusage(RUSAGE_CHILDREN, p);
		fprintf(stdout, "maximum resident set size              =%ld\n", p->ru_maxrss);
		fprintf(stdout, "integral resident set size             =%ld\n", p->ru_idrss);
		fprintf(stdout, "page faults not requiring physical I/O =%ld\n", p->ru_minflt);
		fprintf(stdout, "page faults requiring physical I/O     =%ld\n", p->ru_majflt);
		fprintf(stdout, "swaps                                  =%ld\n", p->ru_nswap);
		fprintf(stdout, "block input operations                 =%ld\n", p->ru_inblock);
		fprintf(stdout, "block output operations                =%ld\n", p->ru_oublock);
		fprintf(stdout, "messages sent                          =%ld\n", p->ru_msgsnd);
		fprintf(stdout, "messages received                      =%ld\n", p->ru_msgrcv);
		fprintf(stdout, "signals received                       =%ld\n", p->ru_nsignals);
		fprintf(stdout, "voluntary context switches             =%ld\n", p->ru_nvcsw);
		fprintf(stdout, "involuntary context switches           =%ld\n", p->ru_nivcsw);
  	return 1;
}

int main(int argc, char *argv[])
{
    char *p=cmd;
    int i=1;
    ptrdiff_t d=ARG_MAX;

    for(i=1; i < argc; i++)
    {
        size_t len=strlen(argv[i]);
        if( d - len > ARG_MAX)
        {
           sprintf(p, "%s ", argv[i]);
           d-=len;
           p+=(len +1);
        }
        else
        {
        	errno=E2BIG;
        	perror("Invalid parameter list");
        	exit(1);
        }
    }
    if(*cmd)
    {
	  	system(cmd);
	  	dump();
	  }
	  else
	  {
	  	errno=EINVAL;
	  	perror("cannot execute command");
	  	exit(1);
	  }
    return 0;
}
