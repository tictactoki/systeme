#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>

#include "conf_params.h"

struct thread_mem_struct {
  int thread_id;
  int mem_occupation_perc;
  int duration;
};

volatile int running = 1;
double total_percentage = 0;
pthread_mutex_t percentage_mutex;

static void run_handler(int signo){
  if(signo == SIGABRT || signo == SIGTERM || signo == SIGINT){
    running = 0;
  }
}

void* memory_thread(void* arg){
  struct thread_mem_struct* mem_struct = (struct thread_mem_struct*)arg;
  int perc = mem_struct->mem_occupation_perc, i, j, aborting_time = 1, tmp;
  int duration = mem_struct->duration;
  long long total_memory = GET_MAX_PAGES() * GET_PAGE_SIZE();
  long long memory_to_occupy = (long long)(total_memory/100.0 * perc);
  long long one_percent = (long long)(total_memory/100);
  double ratio_per_sec = perc * 1.0/duration;
  double proc_perc = 0;
  char* allocated_zones[duration];
  size_t allocated_sizes[duration];
  long long elapsed_time = 0;
  
  if(signal(SIGINT, run_handler) == SIG_ERR){
    perror("signal SIGINT");
    exit(0);
  }

  if(signal(SIGABRT, run_handler) == SIG_ERR){
    perror("signal SIGINT");
    exit(0);
  }

  if(signal(SIGTERM, run_handler) == SIG_ERR){
    perror("signal SIGINT");
    exit(0);
  }

  for(i=0; i<duration; i++){   
    /*allocated_sizes[i] = (size_t)(sizeof(char) * (memory_to_occupy < one_percent ? memory_to_occupy : one_percent) * ratio_per_sec);*/
    
    /*
      allocated_sizes[i] = (size_t) (sizeof(char) 
      (memory_to_occupy */

    if(i == duration - 1){
      allocated_sizes[i] = (size_t)(sizeof(char) * memory_to_occupy);
    }
    else{
      allocated_sizes[i] = (size_t)(sizeof(char) * one_percent * ratio_per_sec);
    }
      
    allocated_zones[i] = malloc(allocated_sizes[i]);
      
    if(allocated_zones[i] == NULL){
      memory_to_occupy = 0;
      //TODO: voir quoi faire
      break;
    }
    else{
      memory_to_occupy -= allocated_sizes[i];
      mlock(allocated_zones[i], allocated_sizes[i]);
    }
    
    sleep(1);
    
    proc_perc += ratio_per_sec;
    
    pthread_mutex_lock(&percentage_mutex);
    if(i == duration - 1){
      total_percentage += ratio_per_sec + (perc - proc_perc);
    }
    else{
      total_percentage += ratio_per_sec;
    }
    printf("Time: %d seconds,\t Percentage: %d\n", (i+1), (int)total_percentage);
    pthread_mutex_unlock(&percentage_mutex);
  }

  i = 0;

  while(running){
    if(i >= perc){
      i = 0;
    }

    for(j=0; j<allocated_sizes[i] && running; j++){
      tmp = allocated_zones[i][j];
    }

    i++;
  }

  printf("Program aborting in %d second(s)\n", aborting_time);
  sleep(aborting_time);
  
  for(i=0; i<perc; i++){
    if(allocated_zones[i] != NULL){
      munlock(allocated_zones[i], allocated_sizes[i]);	      
      free(allocated_zones[i]);
    }    
  }
}

void usage(){
  printf("A executer en mode superutilisateur\n");
  printf("./load-memory POURCENTAGE DUREE NB_PROCESSEURS");
}

int main(int argc, char** argv){
  int process_nb = 0, i, res, mem_usage, mem_usage_per_thread = 0,
    mem_usage_bonus = 0, duration = 0;
 
  //Verification des arguments
  if(argc < 4){
    printf("Nombre d'arguments insuffisant\n");
    exit(0);
  }
  
  res = sscanf(argv[1], "%d", &mem_usage);

  if(res != 1 || mem_usage > 100){
    printf("Pourcentage d'occupation memoire incorrect\n");
    usage();
    exit(0);
  }

  res = sscanf(argv[2], "%d", &duration);

  if(res != 1){
    printf("Duree de l'occupation incorrect\n");
    usage();
    exit(0);
  }

  res = sscanf(argv[3], "%d", &process_nb);
  
  if(res != 1){
    printf("Nombre de processeurs incorrect\n");
    usage();
    exit(0);
  }

  if(process_nb > GET_PROCS_NUMBER()){
    printf("Nombre de processeurs a utiliser superieur a ceux disponibles\n");
    exit(0);
  }

  pthread_t threads[process_nb];
  int thread_mem_occup;
  struct thread_mem_struct infos[process_nb];

  //Utilisation memoire d'un process
  mem_usage_per_thread = mem_usage / process_nb;

  /*
    Bonus d'utilisation memoire du dernier processeur
    (si la quantite a occuper n'est pas un multiple
    du nombre de processeurs a utiliser)
  */
  mem_usage_bonus = mem_usage % process_nb;

  pthread_mutex_init(&percentage_mutex, NULL);

  //Creation des threads
  for(i=0; i<process_nb; i++){
    if(i == process_nb - 1 && mem_usage_bonus != 0){
      thread_mem_occup = mem_usage_per_thread + mem_usage_bonus;
    }
    else{
      thread_mem_occup = mem_usage_per_thread;
    }    

    infos[i].thread_id = i;
    infos[i].duration = duration;
    infos[i].mem_occupation_perc = thread_mem_occup;
   
    res = pthread_create(&threads[i], NULL, memory_thread, &infos[i]);

    if(res != 0){
      perror("pthread creation failure");
      exit(0);
    }
  }
  
  //Attente de fin des threads
  for(i=0; i<process_nb; i++){
    pthread_join(threads[i], NULL);
  }

  pthread_mutex_destroy(&percentage_mutex);

  return 0;
}
