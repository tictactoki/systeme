#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/resource.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include <errno.h>


double interval;
double var = 100000;
struct rusage ru;
struct timeval ru_time;
struct timespec start, end;
double times_c,core_number,cpu_time,cpu,rest;
unsigned int secondes,percentages;
int flags,core;
pthread_t *tab;



/*
  Retourne la valeur correspondante à number
  Retourne 1 sinon
*/
unsigned int convert(char * number){
  char *endptr;
  errno = 0;
long val = strtol(number,&endptr,10);
if(errno == ERANGE){
    printf("out of bound\n");
    return 1;
  }
  if(errno == EINVAL){
    printf("unsupported value\n");
    return 1;
  } 
  if(val <= 0){
    printf("not valid number\n");
    return 1;
  }
  if(endptr == number){
    printf("No digits were found\n");
    return 1;
  }
  return (unsigned int)val;
}


void increase(){
  if(clock_gettime(CLOCK_REALTIME,&start) == -1){
    perror("clock\n");
    exit(0);
  }
  int i;
  while(1){
    
    for(i=0;i<var;i++){
    }
    var+=500000;
    if(clock_gettime(CLOCK_REALTIME,&end) == -1){
      perror("clock\n");
      exit(0);
    }
    times_c = ((double)end.tv_sec - (double)start.tv_sec) + (((double)end.tv_nsec - (double)start.tv_nsec)/ 1000000000.0);
    if(getrusage(RUSAGE_SELF,&ru) == -1){
      perror("getrusage\n");
      exit(0);
    }
    ru_time = ru.ru_utime;
    printf("%ld %ld\n",ru_time.tv_sec,ru_time.tv_usec);
    cpu_time =(double) ru_time.tv_sec + ((double)ru_time.tv_usec / 1000000.0);
    //clock_gettime(CLOCK_REALTIME,&end);
    // time = ((double)end.tv_sec - (double)start.tv_sec) + (((double)end.tv_nsec - (double)start.tv_nsec)/ 1000000000.0);
    cpu = (cpu_time / times_c / core_number) * 100;
    printf("temps:%d s | temps passé: %f | cpu usage:%f%%\n",secondes,times_c,cpu);
    //clock_gettime(CLOCK_REALTIME,&start);
    usleep(50000);
    //usleep(1000000/(interval*mul));
    if(cpu >= percentages)break;
  }
}

void * fonction_thread(void * s){
  increase();
  pthread_exit(0);
}

int main (int argc, char ** argv){

  flags = argc-1;

  if(flags == 3){
    secondes = convert(argv[2]);
    percentages = convert(argv[1]);
    core = convert(argv[3]);
  }
  else{
    secondes = 10;
    percentages = 10;
    core = 1;
  }
  interval = percentages/secondes;
  
  
  core_number = sysconf(_SC_NPROCESSORS_ONLN);
  if(core > core_number){
    perror("core number\n");
    exit(0);
  }
  else {
    core_number = core;
  }
  //pthread_t thread1 ,thread2;
  // pthread_create(&thread1,NULL,fonction_thread,NULL);
  //pthread_create(&thread2,NULL,fonction_thread,NULL);
  //pthread_join(thread1,NULL);
  //pthread_join(thread2,NULL);
  
  increase();

  if(clock_gettime(CLOCK_REALTIME,&end) == -1){
    perror("clock\n");
    exit(0);
  }
  times_c = ((double)end.tv_sec - (double)start.tv_sec) + (((double)end.tv_nsec - (double)start.tv_nsec)/ 1000000000.0);

  rest = (double)secondes - times_c;

  usleep(rest*1000000);
  
  printf("rest: %f\n",rest);
  
  printf("final: %f %%\n",cpu);
  
  
  return 0;
  
  
}
