int read_nr_cpus(int *cpurealid) {
	FILE *cpuinfofile;
	int cpunum = 0;
	
	if ((cpuinfofile = fopen("/proc/cpuinfo","r")) == NULL ) {
		printf("could not find /proc/cpuinfo!!!\n");
	} else {
		char *buffer;
		int nchars;
		buffer = calloc(BUFFSIZE,sizeof(ONEBITE)); 
		while (!feof(cpuinfofile)) 
		{
			fscanf(cpuinfofile,"%s       : %d",buffer,&nchars);
			if ((strcmp(PROCESSOR,buffer) == 0)) {
				*cpurealid = nchars;
				cpurealid++;
				cpunum++;
			}
		}
		free(buffer); 
	}
	return(cpunum);			
}
