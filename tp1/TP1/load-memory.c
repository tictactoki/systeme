#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

struct thread_mem_struct {
  int thread_id;
  int mem_occupation_perc;
  int duration;
};


void* memory_thread(void* arg);
long get_number_of_processors(void);


pthread_mutex_t memory_mutex;


void* memory_thread(void* arg){
  struct thread_mem_struct* mem_struct = (struct thread_mem_struct*)arg;
  int running = 1;
  int exec_time = 0;
  int steps = mem_struct->duration / mem_struct->mem_occupation_perc 
    + (mem_struct->duration % mem_struct->mem_occupation_perc == 0 ? 0 : 1);
  int sleep_time = mem_struct->duration / mem_struct->mem_occupation_perc;
  int sleep_time_bonus = mem_struct->duration % mem_struct->mem_occupation_perc;
  int i;
  
  //while(exec_time < mem_struct->duration){
  for(i=0; i<steps; i++){
    //pthread_mutex_lock(&memory_mutex);
    
    //TODO: occuper l'espace memoire

    if(i == steps - 1){
      sleep(sleep_time);
    }
    else{
      sleep(sleep_time + sleep_time_bonus);
    }
    //exec_time++;

    //pthread_mutex_unlock(&memory_mutex);
  }
}

long get_number_of_processors(){
  return sysconf(_SC_NPROCESSORS_ONLN);
}

int main(int argc, char** argv){
  int processNb = 0, i, res, mem_usage, mem_usage_per_thread = 0,
    mem_usage_bonus = 0, duration = 0;
 
  //TODO: Verification des arguments
  if(argc < 4){
    printf("Nombre d'arguments insuffisant\n");
    exit(0);
  }
  
  res = sscanf(argv[1], "%d", &mem_usage);

  if(res != 1){
    printf("Pourcentage d'occupation memoire incorrect\n");
    exit(0);
  }

  res = sscanf(argv[2], "%d", &duration);

  if(res != 1){
    printf("Duree de l'occupation incorrect\n");
    exit(0);
  }

  res = sscanf(argv[3], "%d", &processNb);
  
  if(res != 1){
    printf("Nombre de processeurs incorrect\n");
    exit(0);
  }

  if(processNb > get_number_of_processors()){
    printf("Nombre de processeurs a utiliser superieur a ceux disponibles\n");
    exit(0);
  }

  pthread_t threads[processNb];
  int thread_mem_occup;
  struct thread_mem_struct infos[processNb];

  //Utilisation memoire d'un process
  mem_usage_per_thread = mem_usage / processNb;

  /*
    Bonus d'utilisation memoire du dernier processeur
    (si la quantite a occuper n'est pas un multiple
    du nombre de processeurs a utiliser)
  */
  mem_usage_bonus = mem_usage % processNb;

  pthread_mutex_init(&memory_mutex, NULL);

  //Creation des threads
  for(i=0; i<processNb; i++){
    if(i == processNb - 1 && mem_usage_bonus != 0){
      thread_mem_occup = mem_usage_per_thread + mem_usage_bonus;
    }
    else{
      thread_mem_occup = mem_usage_per_thread;
    }    

    infos[i].thread_id = i;
    infos[i].duration = duration;
    infos[i].mem_occupation_perc = thread_mem_occup;
   
    res = pthread_create(&threads[i], NULL, memory_thread, &infos[i]);

    if(res != 0){
      perror("pthread creation failure");
      exit(0);
    }
  }
  
  //Attente de fin des threads
  for(i=0; i<processNb; i++){
    pthread_join(threads[i], NULL);
  }

  pthread_mutex_destroy(&memory_mutex);

  return 0;
}
