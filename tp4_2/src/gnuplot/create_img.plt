# image.png
set terminal "png"

# données des fichiers séparé par des virgules
set datafile separator ","

# création de l'image montrant l'utilisation du cpu en pourcentages/secondes
set output "cpu_per.png"
set xlabel "secondes"
set ylabel "percentages"
plot "../data/cpu.log" using 1:2 with linespoint title "user",\
"../data/cpu.log" using 1:3 with linespoint title "nice",\
"../data/cpu.log" using 1:4 with linespoint title "sys"

# création de l'image load_avg
set output "cpu_avg.png"
set ylabel "load average (percentages)"
plot "../data/cpu.log" using 1:6 with linespoint title "1 min",\
"../data/cpu.log" using 1:7 with linespoint title "5 min",\
"../data/cpu.log" using 1:8 with linespoint title "15 min"

# changement de contextes
set output "cpu_ctxt.png"
set ylabel "contexts"
plot "../data/cpu.log" using 1:9 with linespoint title "contexts"

# nombre de threads
set output "cpu_thread.png"
set ylabel "threads"
plot "../data/cpu.log" using 1:5 with linespoint title "threads"

# mémoire utilisé KB/secondes
set output "memory.png"
set ylabel "memories KB"
plot "../data/memory.log" using 1:2 with linespoint title "total",\
"../data/memory.log" using 1:3 with linespoint title "free",\
"../data/memory.log" using 1:4 with linespoint title "used"

# mémoire utilisé pourcentages/secondes
set output "memory_per.png"
set ylabel "memories (percentages)"
plot "../data/memory.log" using 1:7 with linespoint title "free",\
"../data/memory.log" using 1:8 with linespoint title "used"

# diskstats
set output "diskstats.png"
set ylabel "read, written"
plot "../data/disk.log" using 1:2 with linespoint title "read",\
"../data/disk.log" using 1:3 with linespoint title "written"

# network
set output "network.png"
set ylabel "recv,send"
plot "../data/network.log" using 1:2 with linespoint title "receive",\
"../data/network.log" using 1:3 with linespoint title "send"

# swap
set output "swap.png"
set ylabel "swap"
plot "../data/memory.log" using 1:5 with linespoint title "swap in",\
"../data/memory.log" using 1:6 with linespoint title "swap out"