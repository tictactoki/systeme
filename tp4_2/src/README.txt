Le répertoire src contient les fichiers .c et .h.
Après traitement et récupération des données, le fichier run.c execute le script create_img.plt.

Le répertoire data contient les fichiers .log après exécution du programme.

Le répertoire gnuplot contient un script gnuplot (create_img.plt).
Ce script permet de créer des images à partir des données .log.

lancement: (l'intervalle est de 15 secondes)
	
	NB: nombres de secondes voulu

	make
	./monitoring NB

OU

	make
	./monitoring

Ici le programme récupère les données pendants 15 minutes
