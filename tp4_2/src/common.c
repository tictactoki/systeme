#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "common.h"

struct data_t alloc(){
  struct data_t data;

  data.value = malloc(BUF);
  
  if(data.value == NULL){
    perror("data->value malloc\n");
    exit(0);
  }

  return data;
}


void set_data(struct data_t *data, unsigned short type, void* value){
  data->type = type;
  data->value= value;
}

/*
  Retourne 1 si c'est un nombre 0 sinon
 */
unsigned int convert(char * number){

  char *endptr;
  long val = 0;
  val = strtol(number,&endptr,10);
  
  if(val <= 0 || endptr == number){
    return 15*60;
  }
  
  return val;
}
