#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"
#include "fifo.h"
#include"memory.h"


void calcul_swap(unsigned long long * swap_in,
	    unsigned long long * swap_out,
	    struct data_mem previous,
	    struct data_mem last){

  *swap_in = last.swap_in - previous.swap_in;
  *swap_out = last.swap_out - previous.swap_out;

}

struct data_mem data_memory(){
  
  struct sysinfo sys_info;
  struct data_mem data;

  if(sysinfo(&sys_info) != 0){
    perror("sysinfo");
    exit(0);
  }

  // total memory
  data.total = sys_info.totalram *(value_type)sys_info.mem_unit/ 1024;
  // free memory
  data.free = sys_info.freeram *(value_type)sys_info.mem_unit/ 1024;
  // used memory
  data.used = (sys_info.totalram - sys_info.freeram) *(value_type)sys_info.mem_unit/ 1024;
  // swap in
  data.swap_in = (sys_info.totalswap - sys_info.freeswap) *(value_type)sys_info.mem_unit/ 1024;
  // swap out
  data.swap_out = (sys_info.freeswap) *(value_type)sys_info.mem_unit/ 1024;
  
  // pourcentage
  data.per_free = (double) (100 * data.free / data.total);
  data.per_used = (double) (100 * data.used / data.total);
  
  return data;

}

char * calcul_mem(unsigned int total,
		  struct data_mem previous,
		  struct data_mem last){
  
  unsigned long long swap_in,swap_out;
  char * data_s = malloc(sizeof(char) * SIZE);
  
  if(data_s == NULL){
    perror("data_s mallos memory\n");
    exit(0);
  }

  calcul_swap(&swap_in,&swap_out,previous,last);
  
  // met les valeurs dans data_s
  if(snprintf(data_s,SIZE,"%d,%llu,%llu,%llu,%llu,%llu,%f,%f\n",
	      total,last.total,last.free,last.used,swap_in,swap_out,
	      last.per_free,last.per_used) < 0)
    {
      perror("snprintf memory\n");
      exit(0);
  }
  
  return data_s;
}


void* memory(void* args){
	
  struct data_t data = alloc();
  unsigned int total = 0;
  struct data_mem prev,last;
  int FD = (intptr_t) args;
  char * data_s = NULL;
  
  prev = data_memory();
  
  while(total<total_time) {
    last = data_memory();
    
    data_s = calcul_mem(total,prev,last);

    set_data(&data,MEMORY,(void *) data_s);
    send(&data,FD);

    sleep(INTERVAL);
    total += INTERVAL;

    free(data_s);

    prev = last;

  }
  
  
  exit(0);
}
