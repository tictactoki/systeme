/*
  WONG Stéphane
  SULJOVIC Enis
  LEBEAU Christophe
  RICHARD Marc-Anthony
 */

#ifndef _DISKH_
#define _DISKH_

// structure pour /proc/diskstats
struct data_disk {
  
  unsigned long long read,written;

};

// thread envoyant les données via le fifo
void* disk(void* args);

// initialisation des valeurs à 0
void init_data_disk(struct data_disk * data);

/* retourne la chaine contenant les données
   qui seront transmise via fifo
*/
char * calcul_disk(unsigned int total,
		   struct data_disk previous,
		   struct data_disk last);

// stock les données de /proc/diskstats dans la structure
struct data_disk data_disk();

#endif
