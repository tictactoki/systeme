/*
  WONG Stéphane
  SULJOVIC Enis
  LEBEAU Christophe
  RICHARD Marc-Anthony
 */

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "run.h"
#include "common.h"


void * run(void * args){

  int status, dir;
  pid_t pid;
  
  sleep(total_time);
  // on se place dans le repetoire gnuplot
  dir = chdir("../gnuplot");
  
  if(dir < 0){
    perror("chdir");
    exit(0);
  }

  pid = vfork();
  
  if(pid < 0){
    perror("vfork error\n");
    exit(0);
  }
  
  if(pid == 0){
    // execute le script
    execlp("/usr/bin/gnuplot","/usr/bin/gnuplot",
	   "create_img.plt",NULL);
    printf("pd\n");
  }
  else {
    wait(&status);
  }
  exit(0);

}
