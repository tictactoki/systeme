/*
  WONG Stéphane
  SULJOVIC Enis
  LEBEAU Christophe
  RICHARD Marc-Anthony
 */

#ifndef _FIFOH_
#define _FIFOH_

void send(struct data_t* , int );

int create_fifo();

#endif
