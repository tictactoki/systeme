/*
  WONG Stéphane
  SULJOVIC Enis
  LEBEAU Christophe
  RICHARD Marc-Anthony
 */

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "common.h"
#include "fifo.h"
#include "network.h"

void init_data_dev(struct data_dev * data){
  data->send = 0;
  data->recv = 0;
}

char * calcul_network(unsigned int total,
		      struct data_dev previous,
		      struct data_dev last){
  
  unsigned long long send,recv;
  char * data_s = malloc(sizeof(char) * SIZE);

  if(data_s == NULL){
    perror("data_s malloc network\n");
    exit(0);
  }
  
  send = last.send - previous.send;
  recv = last.recv - previous.recv;
  
  if(snprintf(data_s,SIZE,"%d,%llu,%llu\n",
	      total,recv,send) < 0){
    perror("snprintf network\n");
    exit(0);
  }

  return data_s;

}

struct data_dev data_network(){
  
  FILE *fd;
  char * line = NULL;
  size_t length = 0;
  char name[128];
  ssize_t read;
  unsigned long long recv1=0, recv2=0;
  unsigned long long send1=0, send2=0;
  unsigned long long tmp;
  int i;
  struct data_dev data;
  
  init_data_dev(&data);

  fd = fopen("/proc/net/dev","r");
    
  if(fd == NULL){
    perror("fopen network\n");
    exit(0);
  }
  // les deux première ligne ne nous intéresse pas
  for(i=0;i<2;i++){
    getline(&line,&length,fd);
    free(line);
    line = NULL;
    length = 0;
  }
  // pour chaque ligne restante
  while((read = getline(&line,&length,fd)) >= 0){
    // récupère les valeurs qui nous intéresse 
    if(sscanf(line,"%s %llu %llu %llu %llu %llu %llu"
	      "%llu %llu %llu %llu"
	      ,name,&tmp,&recv1,&tmp,&tmp,&tmp,&tmp
	      ,&tmp,&tmp,&tmp,&send1) < 11){

      perror("sscanf read\n");
      exit(0);
    }
    // somme de toutes les valeurs
    recv2 += recv1;
    send2 += send1;
    
    free(line);
    line = NULL;
    length = 0;
  }

  fclose(fd);

  data.send = send2;
  data.recv = recv2;

  return data;
}


void* network(void* args){

  struct data_t data = alloc();
  unsigned int total = 0;
  int FD = (intptr_t) args;
  char * data_s = NULL;
  struct data_dev prev,last;
  

  init_data_dev(&prev);
  init_data_dev(&last);

  prev = data_network();

  while(total<total_time) {
    last = data_network();
    
    data_s = calcul_network(total,prev,last);

    set_data(&data,NETWORK,(void *)data_s);
    send(&data,FD);

    sleep(INTERVAL);
    total += INTERVAL;

    free(data_s);

    prev = last;
    
    init_data_dev(&last);

  }
  exit(0);
}
