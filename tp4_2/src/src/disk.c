/*
  WONG Stéphane
  SULJOVIC Enis
  LEBEAU Christophe
  RICHARD Marc-Anthony
 */

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "common.h"
#include "fifo.h"
#include "disk.h"


void init_data_disk(struct data_disk * data){
  data->read = 0;
  data->written = 0;
}


char * calcul_disk(unsigned int total,
		   struct data_disk previous,
		   struct data_disk last){
  
  unsigned long long read,written;
  char * data_s = malloc(sizeof(char) * SIZE);

  if(data_s == NULL){
    perror("data_s malloc diskstats\n");
    exit(0);
  }
  
  read = last.read - previous.read;
  written = last.written - previous.written;

  if(snprintf(data_s,SIZE,"%d,%llu,%llu\n",
	      total,read,written) < 0){
    perror("snprintf dist\n");
    exit(0);
  }

  return data_s;

}

struct data_disk data_disk(){
    
  FILE *f2;
  struct data_disk data;
  value_type data_read = 0, data_written = 0;
  value_type tmp,tmp_read,tmp_write;
  char * line = NULL;
  size_t length = 0;
  ssize_t read;
  char tmp_s[128];

  init_data_disk(&data);

  f2 = fopen("/proc/diskstats","r");
  
  if(f2 == NULL){
    perror("fopen diskstats\n");
    exit(0);
  }
  while((read = getline(&line,&length,f2)) >= 0){
    if(sscanf(line,"%llu %llu %s"
	      "%llu"
	      "%llu %llu %llu"
	      "%llu",
	      &tmp,&tmp,tmp_s,
	      &tmp_read, // lu
	      &tmp,&tmp,&tmp,
	      &tmp_write) < 8){ // écrit
	perror("sscanf diskstats\n");
	exit(0);
    }
    data_read += tmp_read;
    data_written += tmp_write;
    
    free(line);
    length = 0;
    line = NULL;
    
  }
  
  fclose(f2);

  data.read = data_read;
  data.written = data_written;

  return data;

}

void* disk(void* args){
	
  struct data_disk prev,last;
  unsigned int total = 0;
  struct data_t data = alloc();
  int FD = (intptr_t) args;
  char * data_s = NULL;

  init_data_disk(&prev);
  init_data_disk(&last);
  
  prev = data_disk();

  while(total<total_time) {
    last = data_disk();

    data_s = calcul_disk(total,prev,last);

    set_data(&data,DISK,(void *)data_s);
    send(&data,FD);

    sleep(INTERVAL);	
    total += INTERVAL;

    free(data_s);
    prev = last;
    init_data_disk(&last);
  }

  exit(0);
}
