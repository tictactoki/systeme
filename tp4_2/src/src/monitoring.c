/*
  WONG Stéphane
  SULJOVIC Enis
  LEBEAU Christophe
  RICHARD Marc-Anthony
 */

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "common.h"
#include "fifo.h"
#include "memory.h"
#include "cpu.h"
#include "network.h"
#include "disk.h"
#include "trait.h"
#include "run.h"


int main(int argc, char *argv[]){
	
  int FIFO_FD;
	
  if(argc == 1){
    total_time = 15*60;
  }
  else {
    total_time = convert(argv[1]);
  }

  // le canal de transmission de données entre le 'client' et le 'serveur'
  FIFO_FD = create_fifo();
	
  //un thread pour collecter les données de memoire
  pthread_t pth_memory;
  pthread_create(&pth_memory, NULL, memory, (void*) (intptr_t) FIFO_FD);	
	
  //un thread pour collecter les données de CPU
  pthread_t pth_cpu;
  pthread_create (&pth_cpu, NULL, cpu, (void*) (intptr_t) FIFO_FD);
	
  //un thread pour collecter les données de disque
  pthread_t pth_disk;
  pthread_create (&pth_disk, NULL, disk,(void*) (intptr_t) FIFO_FD);
		
  //un thread pour collecter les données de réseau
  pthread_t pth_network;
  pthread_create (&pth_network, NULL, network, (void*) (intptr_t) FIFO_FD);
	
  //un thread (Le client) qui trait les données collectées	
  pthread_t pth_trait;
  pthread_create (&pth_trait, NULL, trait, (void*) (intptr_t) FIFO_FD);

  // un thread qui atta la fin pour lancer le script dans le repertoire gnuplot
  pthread_t pth_run_script;
  pthread_create(&pth_run_script, NULL, run, (void *) (intptr_t) FIFO_FD);
  
  // attendre les thread qu'ils finissent 
  pthread_join(pth_memory, NULL);
  pthread_join(pth_cpu, NULL);
  pthread_join(pth_disk, NULL);
  pthread_join(pth_network, NULL);
  pthread_join(pth_trait, NULL);
  pthread_join(pth_run_script, NULL);

  return 0;

}


