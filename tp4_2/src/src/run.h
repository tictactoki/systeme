/*
  WONG Stéphane
  SULJOVIC Enis
  LEBEAU Christophe
  RICHARD Marc-Anthony
 */

#ifndef _RUNH_
#define _RUNH_

void * run(void * args);

#endif
