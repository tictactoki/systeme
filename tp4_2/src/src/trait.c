/*
  WONG Stéphane
  SULJOVIC Enis
  LEBEAU Christophe
  RICHARD Marc-Anthony
 */

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include "common.h"
#include "trait.h"

void save(struct data_t *data){

  FILE * file;
  switch(data->type){
  case MEMORY:
    file = fopen(MEMORY_LOG,"a");
    break;
  case CPU:
    file = fopen(CPU_LOG,"a");
    break;
  case NETWORK:
    file = fopen(NETWORK_LOG,"a");
    break;
  case DISK:
    file = fopen(DISK_LOG,"a");
    break;
  default:  
    perror("error switch\n");
    exit(0);
    break;
  }
  
  if(file == NULL){
    perror("open file\n");
    exit(0);
  }

  fwrite(data->value,1,strlen(data->value),file);
  fclose(file);

}

void* trait(void* args){

  struct data_t data = alloc();
  int FIFO_FD = (intptr_t) args;
					
  if ( ( FIFO_FD = open ( FIFO_PATH , O_RDONLY) ) < 0)
    perror( "open error " ) ;
	
  while ( read ( FIFO_FD , &data , sizeof(struct data_t) + BUF ) > 0){
	 	
    // enregistrement des données sur le disque
    
    save(&data);
		
  }
  exit(0);
}
