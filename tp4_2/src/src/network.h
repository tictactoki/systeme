/*
  WONG Stéphane
  SULJOVIC Enis
  LEBEAU Christophe
  RICHARD Marc-Anthony
 */

#ifndef _NETWORKH_
#define _NETWORKH_

// structure pour /proc/net/dev
struct data_dev {
  unsigned long long send,recv;
};

// initialisation des valeurs à 0
void init_data_dev(struct data_dev * data);

/* calcul entre le précédent et le courant
   retourne la chaine qui sera transmise via le mkfifo
 */ 
char * calcul_network(unsigned int total,
		      struct data_dev previous,
		      struct data_dev last);

// récupère les données de /proc/dev/net
struct data_dev data_network();

// thread envoyant les donénes via le mkfifo
void* network(void* args);

#endif
