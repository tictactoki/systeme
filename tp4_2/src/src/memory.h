/*
  WONG Stéphane
  SULJOVIC Enis
  LEBEAU Christophe
  RICHARD Marc-Anthony
 */

#ifndef _MEMORYH_
#define _MEMORYH_

// structure
struct data_mem {
  
  unsigned long long total,free,used,swap_in,swap_out;
  double per_free,per_used;
};

// retourne la chaine qui sera envoyée via le fifo
char * calcul_mem(unsigned int total,
		  struct data_mem previous,
		  struct data_mem last);

// effectue le calcul pour le swap_in et swap_out
void calcul_swap(unsigned long long * swap_in,
		 unsigned long long * swap_out,
		 struct data_mem previous,
		 struct data_mem last);

// récupère les valeurs avec sysinfo
struct data_mem data_memory();

void* memory(void* args);

#endif
