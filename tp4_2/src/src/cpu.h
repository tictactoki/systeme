/*
  WONG Stéphane
  SULJOVIC Enis
  LEBEAU Christophe
  RICHARD Marc-Anthony
 */

#ifndef _CPUH_
#define _CPUH_

// structure pour recuperer les données dans /proc/stat
struct data_stat {
  
  unsigned long long user,
    nice,
    sys,
    idle,
    iowait,
    irq,
    softirq,
    ctxt,
    nb_th;
  double load_avg[3];
};

// donnée de conservation après calcul
struct data_cpu {
  
  double user,nice,sys,load_avg[3];
  unsigned long long  nb_th, ctxt;

};

// Thread envoyant les données via le fifo
void* cpu(void* args);

// recupère les données de /proc/stat
struct data_stat data_cpu();

// retourne la chaine qui sera transmise via le fifo
char * calcul_cpu(unsigned int total,
		  struct data_stat previous,
		  struct data_stat next);

// fait les calculs entre les données courantes et précédentes
struct data_cpu calcul(struct data_stat previous, struct data_stat last);

// initialisation des valeurs à 0
void init_data_stat(struct data_stat * data);

// initialisation des valeurs à 0
void init_data_cpu(struct data_cpu * data);

// retourne l'utilisation totale du cpu
double get_total(struct data_stat data);

#endif
