/*
  WONG Stéphane
  SULJOVIC Enis
  LEBEAU Christophe
  RICHARD Marc-Anthony
 */

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "cpu.h"
#include "common.h"
#include "fifo.h"


void init_data_cpu(struct data_cpu * data){
  int i;
  data->user = 0;
  data->nice = 0;
  data->sys = 0;
  data->nb_th = 0;
  data->ctxt = 0;
  for(i=0;i<3;i++){
    data->load_avg[i] = 0;
  }
}

void init_data_stat(struct data_stat * data){
  int i;
  data->user = 0;
  data->nice = 0;
  data->sys = 0;
  data->idle = 0;
  data->iowait = 0;
  data->irq = 0;
  data->softirq = 0;
  data->ctxt = 0;
  data->nb_th = 0;
  for(i=0;i<3;i++){
    data->load_avg[i] = 0;
  }
}


double get_total(struct data_stat data){
  return (double)(data.user + data.nice + data.sys + data.idle + data.irq + data.softirq);
}

struct data_cpu calcul(struct data_stat previous, struct data_stat last){
  
  double per_user, per_nice, per_sys,load[3],tmp;
  double total = get_total(last) - get_total(previous);
  struct data_cpu data;
  
  init_data_cpu(&data);
  // calcul pourcentage
  per_user = last.user - previous.user;
  per_nice = last.nice - previous.nice;
  per_sys = last.sys - previous.sys; 
  // changement de contexte et nb thread
  data.ctxt = last.ctxt - previous.ctxt;
  data.nb_th = last.nb_th - previous.nb_th;  

  getloadavg(load,3);
  
  tmp = load[0] - previous.load_avg[0];
  
  if(tmp > 0){
    data.load_avg[0] = 100 * tmp;
  }
  
  tmp = load[1] - previous.load_avg[1];
  
  if(tmp > 0){
    data.load_avg[1] = 100 * tmp;
  }

  tmp = load[2] - previous.load_avg[2];

  if(tmp > 0){
    data.load_avg[2] = 100 * tmp;
  }

  (per_user > 0)? (per_user = 100 * per_user / total) : (per_user = 0);
  (per_nice > 0)? (per_nice = 100 * per_nice / total) : (per_nice = 0);
  (per_sys > 0)? (per_sys = 100 * per_sys / total) : (per_sys = 0);
  
  data.user = per_user;
  data.sys = per_sys;
  data.nice = per_nice;
 
  return data;
  
}

char * calcul_cpu(unsigned int total,struct data_stat previous, struct data_stat next){
  
  struct data_cpu data = calcul(previous, next);
  char * data_s = malloc(sizeof(char) * SIZE);

  if(data_s == NULL){
    perror("data_s malloc cpu\n");
    exit(0);
  }

  // stocke dans data_s les données pertinentes
  if(snprintf(data_s,SIZE,"%d,%f,%f,%f,%llu,%f,%f,%f,%llu\n"
	      ,total,data.user,data.nice,data.sys,
	      data.nb_th,data.load_avg[0],
	      data.load_avg[1],
	      data.load_avg[2],data.ctxt) < 0){
    perror("snprintf cpu\n");
    exit(0);
  }

  return data_s;

}


struct data_stat data_cpu(){
  
  FILE *f1;
  char c[10];
  
  value_type value;

  char* line = NULL;
  size_t size = 0;
  ssize_t read;
  char tag[20];
  struct data_stat data;

  init_data_stat(&data);

  f1 = fopen("/proc/stat","r");
  // pour chaque ligne lu
  while((read = getline(&line,&size,f1)) >= 0){
      
    if(size == -1){
      perror("getline");
      exit(0);
    }
      
    if(size == 0){
      break;
    }
    
    sscanf(line, "%s", tag);
    
    if(strcmp(tag, "cpu") == 0){
      if(sscanf(line, "%s %llu %llu %llu %llu %llu %llu %llu",
		c, &data.user, &data.nice, &data.sys,
		&data.idle, &data.iowait,
		&data.irq, &data.softirq) < 8){
	perror("sscanf cpu\n");
	exit(0);
      }
    }
    else if(strcmp(tag, "ctxt") == 0){
      if(sscanf(line, "%s %llu", c, &data.ctxt) < 2){
	perror("sscanf ctxt\n");
	exit(0);
      }
    }
    else if(strcmp(tag, "processes") == 0){
      if(sscanf(line, "%s %llu", c, &data.nb_th) < 2){
	perror("sscanf processes\n");
	exit(0);
      }
    }

    size = 0;
    free(line);
    line = NULL;
  }
 	
  fclose(f1); // ferme proc stat
  // ou recuperer dans /proc/loadavg
  getloadavg(data.load_avg,3);

  return data;

};

void* cpu(void* args){
	
  int FD = (intptr_t) args;
  unsigned int total = 0;
  struct data_t data = alloc();
  char * data_s = NULL;
  struct data_stat prev,last;

  prev = data_cpu();

  // traitement
  while(total<total_time) {
    last = data_cpu();
    
    data_s = calcul_cpu(total,prev,last);

    set_data(&data,CPU,(void *) data_s);
    send(&data,FD);
    
    sleep(INTERVAL);
    total += INTERVAL;
    
    free(data_s);

    prev = last;

  }
  exit(0);
}

