/*
  WONG Stéphane
  SULJOVIC Enis
  LEBEAU Christophe
  RICHARD Marc-Anthony
 */

#ifndef _COMMONH_
#define _COMMONH_
#include <stdio.h>
#include <time.h>

#define FIFO_PATH "data.fifo"
#define INTERVAL 15 // en seconde
#define SIZE 4096

unsigned int total_time; // temps total du monitoring
enum type {
  // type of data to send
  SYSTEM, MEMORY, CPU, DISK, NETWORK
 
};

#define	CPU_LOG "../data/cpu.log"
#define	MEMORY_LOG "../data/memory.log"
#define	NETWORK_LOG "../data/network.log"
#define	DISK_LOG "../data/disk.log"


#define value_type unsigned long long
#define BUF sizeof(value_type)

struct data_t {
  unsigned short type;
  void* value;
};


struct data_t alloc();

void set_data(struct data_t *, unsigned short, void* );

unsigned int convert(char * number);

#endif
