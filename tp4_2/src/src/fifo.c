/*
  WONG Stéphane
  SULJOVIC Enis
  LEBEAU Christophe
  RICHARD Marc-Anthony
 */

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include "common.h"
#include "fifo.h"

static pthread_mutex_t my_mutex = PTHREAD_MUTEX_INITIALIZER;

void send(struct data_t* data, int FIFO_FD){
	pthread_mutex_lock (&my_mutex);
	write ( FIFO_FD , data , sizeof ( struct data_t ) + BUF );
	pthread_mutex_unlock (&my_mutex);
}

int create_fifo(){
	int FIFO_FD;
	
	remove(FIFO_PATH);	
	
	if ( mkfifo ( FIFO_PATH , S_IRUSR | S_IWUSR ) < 0 )
		perror("mkfifo error") ;
	
	if ( ( FIFO_FD = open ( FIFO_PATH , O_RDWR ) ) < 0)
		perror("open error ") ;
		
	return FIFO_FD;	
}
