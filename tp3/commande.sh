# Stéphane Wong & Christophe Lebeau
#!/bin/sh
#création des repertoires pour le nouveau chroot
#Copie les librairies avec les dépendances dans bin et lib (indiquées par ldd)
echo "création des librairies et dependances"

mkdir ./bin
mkdir ./lib
mkdir ./proc
cmd="sudo bash ls cp uname rm mkdir cat rmdir ps touch"

for exec in $cmd; do
    path=`which $exec`
    cp $path ./bin
done

for exec in $cmd; do
    path=`which $exec`
    lib=$( ldd $path | sed "s/ *(.*)//" | sed "s/.*=> *//" )
    cp $lib ./lib
done

echo "création terminé"

