/**
   Wong Stéphane & Christophe Lebeau
 **/
#define _GNU_SOURCE
#include <sys/wait.h>
#include <sys/utsname.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <sched.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>


#define STACK_SIZE (1024 * 1024)

// communication 
int pipecom[2];


static int childFunc(void *arg) {

  pid_t pid;
  struct utsname uts;
  char car;
  //changer le hostname
  sethostname(arg, strlen(arg));
  
  //afficher le hostname
  uname(&uts);

  printf("======================= CLONE FILS ====================\n");

  printf("hostname dans clone:  %s\n", uts.nodename);
  system("uname -a");
  sleep(1);

  close(pipecom[1]);

  read(pipecom[0],&car,1);
  
  printf("DEPUIS LE PERE J'AI RECU PAR LE PIPE: %c\n",car);

  pid = fork();
  if(pid < 0){
    perror("error fork\n");
    return 0;
  }
  
  else if (pid == 0){
    // Ajoute un nouvel utilisateur
    system("useradd FrancoisArmand 2> /dev/null");
 
    if(execl("/bin/uname", "uname", "-n", NULL) == -1 ){
      perror("uname\n");
      return 0;
      }
    
  }
  waitpid(pid, NULL, 0); 

  printf("Dans Clone: getpid():%d | getppid():%d | childPID:%d\n",getpid(), getppid(), pid);
  // nouveau chroot
  if (chroot(".") == -1){
    perror("chroot\n"); 
    return 0;
  }
  
  // montage des processus dans le nouvel espace
  if(mount("proc","/proc","proc",0,NULL) == -1){
    perror("mount\n");
    return 0;
  }

  // On s'assure qu'on est bien à la nouvelle racine
  if(chdir("/") == -1){
    perror("chdir\n");
    return 0;
  }

  printf("dirname: %s\n",get_current_dir_name());

  if(execl("./bin/bash", "bash", NULL) == -1 ){
    perror("bash\n"); 
    return 0;
  }
  
  return 0;
}

int main(int argc, char *argv[]){
  char *stack;
  pid_t pid;
  int pipefd;

  if (argc < 2) {
    printf("ecrire le nouveau hostname du fils\n");
    exit(0);
  }
  
  printf("==================== PERE ====================\n");

  printf("dirname: %s\n",get_current_dir_name());

  printf("pid = %d\n",getpid());
  
  stack = malloc(STACK_SIZE);
  if(stack == NULL){
    perror("malloc stack\n");
    exit(0);
  }
  
  // création du pipe
  pipefd = pipe(pipecom);

  if(pipefd == -1){
    perror("pipe\n");
    exit(0);
  }
  // Presque une "nouvelle machine" option CLONE_NEWNET manquant également
  pid = clone(childFunc, stack + (STACK_SIZE), CLONE_NEWNS | CLONE_NEWUTS | CLONE_NEWPID | CLONE_NEWIPC |  SIGCHLD, argv[1]);
  
  if(pid == -1){
    perror("clone\n");
    free(stack);
    exit(0);
  }

  sleep(2);
  // transmission du caractère P
  close(pipecom[0]);
  write(pipecom[1],"P",1);

  waitpid(pid, NULL, 0);
  
  printf("==================== RETOUR PERE ====================\n");
  system("uname -a");
  printf("getpid(): %d | childpid(): %d\n",getpid(),pid);
  
  free(stack);
  return 0;
}
